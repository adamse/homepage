---
author: Ben Gamari
title: "GHC 8.4.3 released"
date: 2018-05-29T20:08:58
tags: release
---

Hello everyone,

The GHC team is pleased to announce the availability of GHC 8.4.3. The
source distribution, binary distributions, and documentation for this
release are available [here](https://downloads.haskell.org/~ghc/8.4.3).

This release includes a few bug fixes including:

 * A code generation bug resulting in crashing of some programs using
   `UnboxedSums` has been fixed (#15038).

 * #14381, where Cabal and GHC would disagree about `abi-depends`,
   resulting in build failures, has been worked around. Note that the
   work-around patch has already been shipped by several distributions
   in previous releases, so this change may not be visible to you.

 * By popular demand, GHC now logs a message when it reads a package
   environment file, hopefully eliminating some of the confusion wrought
   by this feature.

 * GHC now emits assembler agreeable to newer versions of Gnu binutils,
   fixing #15068.

 * `SmallArray#`s can now be compacted into a compact region

Thanks to everyone who has contributed to developing, documenting, and
testing this release!

As always, let us know if you encounter trouble.
