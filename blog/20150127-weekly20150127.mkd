---
author: thoughtpolice
title: "GHC Weekly News - 2015/01/27"
date: 2015-01-27T21:03:00
tags: ghc news
---

Hi \*,

It's time for some GHC Weekly news!

 - Austin took the time the past week to check `./validate
   --slow` failures, and is planning on filing bugs and fixes for the
   remaining failures soon. Afterwords, we'll immediately begin
   enabling `--slow` on Phabricator, so developers get their patches
   tested more thoroughly.

 - The 7.10 release looks like it will likely not have a 3rd Release
   Candidate, and will be released in late Feburary of 2015, as we
   originally expected.

 - The 7.10 branch currently has two showstopping bugs we plan on
   hitting before the final release. And we'd really like for users
   to test so we can catch more!

 - Austin Seipp will likely be gone for the coming week in a trip to
   New York City from the 28th to the 4th, meaning (much to the
   dismay of cheering crowds) you'd better catch him beforehand if
   you need him! (Alternatively Austin will be held back due to an
   intense snowstorm developing in NYC. So, we'll see!)

 - Austin is planning on helping the LLVM support in
   HEAD soon; after coordinating with Ben Gamari, we're hoping to
   ship GHC 7.12 with (at least) LLVM 3.6 as an officially supported
   backend, based on the documentation described in
   <https://ghc.haskell.org/trac/ghc/wiki/ImprovedLLVMBackend> - lots
   of thanks to Ben for working with upstream to file bugs and
   improve things!

And in other news, through chatter on the mailing list and Phabricator, we have:

 - Austin Seipp announced GHC 7.10.1 RC2: <https://www.haskell.org/pipermail/ghc-devs/2015-January/008140.html>

 - Peter Trommler posted his first version of a native Linux/PowerPC
   64bit code generator! There's still a lot more work to do, but
   this is a significantly improved situation over the unregisterised
   C backend. Curious developers can see the patch at Phab:D629.

 - A long, ongoing thread started by Richard Eisenberg about the
   long-term plans for the vectorisation code have been posted. The
   worry is that the vectoriser as well as DPH have stagnated in
   development, which costs other developers any time they need to
   build GHC, make larger changes, or keep code clean. There have
   been a lot of varied proposals in the thread from removing the
   code to commenting it out, to keeping it. It's unclear what the
   future holds, but the discussion still rages on. <https://www.haskell.org/pipermail/ghc-devs/2015-January/007986.html>

 - Karel Gardas is working on reviving the SPARC native code
   generator, but has hit a snag where double float load instructions
   were broken. <https://www.haskell.org/pipermail/ghc-devs/2015-January/008123.html>

 - Alexander Vershilov made a proposal to the GHC team: can we remove
   the `transformers` dependency? It turns out to be a rather painful
   dependency for users of the GHC API and of packages depending on
   `transformers`, as you cannot link against any version other than
   the one GHC ships, causing pain. The alternative proposal involves
   splitting off the `transformers` dependency into a package of
   Orphan instances. The final decision isn't yet clear, nor is a
   winner in clear sight yet! <https://www.haskell.org/pipermail/ghc-devs/2015-January/008058.html>

 - Konstantine Rybnikov has a simple question about GHC's error
   messages: can they say `Error:` before anything else, to be more
   consistent with warnings? It seems like a positive change - and it
   looks like Konstantine is on the job to fix it, too. <https://www.haskell.org/pipermail/ghc-devs/2015-January/008105.html>

 - Simon Marlow has started a long thread about the fate of records
   in future GHC versions. Previously, Adam Gundry had worked on
   `OverloadedRecordFields`. And now Nikita Volkov has introduced his
   `records` library which sits in a slightly different spot in the
   design space. But now the question is - how do we proceed? Despite
   all prior historical precedent, it looks like there's finally some
   convergence on a reasonable design that can hit GHC in the future. <https://www.haskell.org/pipermail/ghc-devs/2015-January/008049.html>

Closed tickets the past two weeks include: #9889, #9384, #8624, #9922, #9878, #9999, #9957, #7298, #9836, #10008, #9856, #9975, #10013, #9949, #9953, #9856, #9955, #9867, #10015, #9961, #5364, #9928, and #10028.
