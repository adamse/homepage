---
author: thoughtpolice
title: "GHC Weekly News - 2015/02/10"
date: 2015-02-10T23:14:44
tags: ghc news
---

Hi \*,

Welcome! This is the first GHC Weekly news of February 2015. You might be wondering what happened to the last one. Well, your editor was just in New York for the past week attending [Compose Conference](http://www.composeconference.org), making friends and talking a lot about Haskell (luckily we missed a snow storm that may have messed it up quite badly!)

The conference was great. I got to have some interesting discussions about GHC and Haskell with many friendly faces from all around at an incredibly well run conference with a stellar set of people. Many thanks to NY Haskell (organizing), Spotify (hosting space), and to all the speakers for a wonderful time. (And of course, your editor would like to thank his employer Well-Typed for sending him!)

But now, since your author has returned, GHC HQ met back up this week for some
discussion, with some regularly scheduled topics. For the most part it was a short meeting this week - our goals are pretty well identified:

 - GHC HQ and the Core Libraries Committee have posted a survey on the future of the 7.10 prelude and the FTP/BBP discussion. The deadline is February 20th, so please vote if the discussion is of interest to you. Simon Peyton-Jones and Simon Marlow will be making the final decision. <https://www.haskell.org/pipermail/haskell-cafe/2015-February/118095.html>

 - It's likely GHC HQ will do a third 7.10.1 Release Candidate at the very end of February after the votes are included. We missed some patches in RC2 (such as Phab:D347) and incorporated even more bugfixes, so this is worth a test drive by users.

 - For the most part, things for 7.10 have been going very smoothly other than the debates and a few bugs trickling in - there has not been much ticket activity the past two weeks, so things feel pretty good right now. Austin will mostly be focused on shipping 7.10 and keeping the normal review/patch/triaging cycles going until it's done. We're on track to fix all the major bugs we've assigned (see milestone:7.10.1).

Since my last post, we've also had other random assorted chatter on the mailing lists by the dev team:

 - In light of a recent large bug in GHC which can be used to derive
   `unsafeCoerce`, GHC HQ **has decided to push back the 7.10
   release a bit longer to about March**, in order to fix this bug
   and ferret out the little fallout afterwords. It turns out this
   isn't a simple bug to fix, but luckily a fix is being worked on
   already. <https://www.haskell.org/pipermail/ghc-devs/2015-January/008189.html>

 - Luckily, Iavor has started work on fixing this nasty bug, and had
   a few questions for the list:
   <https://www.haskell.org/pipermail/ghc-devs/2015-February/008269.html>

 - Iavor Diatchki has raised a new topic about a simpler
   OverloadedRecordsField proposal. Adam swooped in to address some
   points about the
   design. <https://www.haskell.org/pipermail/ghc-devs/2015-January/008183.html>

 - Herbert Valerio Riedel posted about a huge (76x) regression
   between GHC 7.11 and GHC 7.10, but strangely nobody has picked up
   as to why this is the case yet! <https://www.haskell.org/pipermail/ghc-devs/2015-January/008207.html>

 - David Feuer has a question: why is `undefined` so special? In
   particular, it seems as if `undefined` can be specially used as a
   value with a type of kind `#` as well as `*`. It turns out GHC has
   a special notion of subkinding, and `undefined` has a type more
   special than meets the eye which allows this, as Adam Gundry
   replied. <https://www.haskell.org/pipermail/ghc-devs/2015-February/008222.html>

 - Merijn Verstraaten has started up a discussion about a new
   proposal of his, ValidateMonoLiterals. The proposal revolves
   around the idea of using GHC to enforce compile-time constraints
   on monomorphic literals, whose type may have invariants enforced
   on them. While this is doable with Template Haskell, Merijn would
   like to see something inside GHC instead. <https://www.haskell.org/pipermail/ghc-devs/2015-February/008239.html>

 - David Feuer asked: can we merge `FlexibleContexts` with
   `FlexibleInstances`? The proposal seems to be relatively
   undiscussed at the moment with a neutral future, but perhaps
   someone would like to chime in on this minor issue. <https://www.haskell.org/pipermail/ghc-devs/2015-February/008245.html>

 - Greg Weber opened up a discussion about 'Restricted Template
   Haskell', which would hopefully make it easier for users to see
   what a TH computation is actually doing. It turns out - as noted
   by Simon - that Typed Template Haskell is perhaps closer to what
   Greg wants. The proposal and discussion then resulted in us
   realising that the typed TH documentation is rather poor!
   Hopefully Greg or someone can swing in to improve things. <https://www.haskell.org/pipermail/ghc-devs/2015-February/008232.html>

Closed tickets the past two weeks include: #10028, #10040, #10031, #9935, #9928, #2615, #10048, #10057, #10054, #10060, #10017, #10038, #9937, #8796, #10030, #9988, #10066, #7425, #7424, #7434, #10041, #2917, #4834, #10004, #10050, #10020, #10036, #9213, and #10047.
