---
author: thoughtpolice
title: "GHC Weekly News - 2015/03/24"
date: 2015-03-25T02:20:19
tags: ghc news
---

Hi \*,

It's time for the GHC weekly news. We've had an absence of the last one, mostly
due to a lot of hustle to try and get 7.10 out the door (more on that shortly
throughout this post). But now we're back and everything seems to be taken
care of.

This week, in the wake of the GHC 7.10 release (which is occuring EOD,
hopefully), GHC HQ met up for a brief chat and caught up:

 - This week GHC HQ met for only a very short time to discuss the pending
 release - it looks like all the blocking bugs have been fixed, and we've got
 everything triaged appropriately. You'll hopefully see the 7.10 announcement
 shortly after reading this.

We've also had small amounts of list activity (the past 6-8 weeks have been very, very quiet it seems):

 - Your editor sent out a call for developers to fill information in on the
 Status page about what they plan to do. If you're working on something, please
 add it there!
 <https://mail.haskell.org/pipermail/ghc-devs/2015-March/008661.html>

 - Herbert Valerio Riedel asked about a possible regression regarding
 identifiers containing unicode subscripts - but nobody has replied!
 <https://mail.haskell.org/pipermail/ghc-devs/2015-March/008503.html>

 - Doug Burke chimed in as a new contributor and wrote down some notes on what
 it took to write his first patch and submit it to us - and we really
 appreciate the feedback, Doug!
 <https://mail.haskell.org/pipermail/ghc-devs/2015-March/008526.html>

 - Yitzchak Gale revived a thread he started a while back, which puttered out:
 bootstrapping GHC 7.8 with GHC 7.10. The long and short of it is, it should
 just about work - although we still haven't committed to this policy, it looks
 like Yitz and some others are quite adamant about it.
 <https://mail.haskell.org/pipermail/ghc-devs/2015-March/008531.html>

 - Neil Mitchell uncovered a nasty bug in GHC 7.10.1 RC3, submitted it to us.
 He also wrote a fantastic [blog post explaining the
 issue](http://neilmitchell.blogspot.co.at/2015/03/finding-ghc-bug.html). And
 it was promply diagnosed, fixed, and taken care of by our own Joachim
 Breitner. Thank you for the fast response Joachim and Neil!
 <https://mail.haskell.org/pipermail/ghc-devs/2015-March/008532.html>

 - Mark Lentczner has announced Alpha releases of the Haskell Platform
 2015.2.0.0, containing GHC 7.10.1 RC3:
 <https://mail.haskell.org/pipermail/ghc-devs/2015-March/008597.html>

 - Simon Peyton Jones asks: what's the current state about having simultaneous
 installations of a package? Simon is a bit confused as to why this is still a
 problem when we have all the tools to solve it, it looks like! (But read on
     for more):
 <https://mail.haskell.org/pipermail/ghc-devs/2015-March/008602.html>

 - Michael Snoyman asks: can we get a new feature patch in GHC 7.10.2? The
 answer seems to be an unfortunate 'no', but with some tips, Michael may end up
 backporting the changes from HEAD to GHC 7.10 himself.
 <https://mail.haskell.org/pipermail/ghc-devs/2015-March/008612.html>

Some noteworthy commits that went into `ghc.git` in the past two weeks include:

 - Commit 71fcc4c096ec0b575522e4c2d0104ef7a71a13c5 - GHC defaults to using the `gold` linker on ARM/Android and ARM/Linux targets.
 - Commit 9dfdd16a61e79cb03c633d442190a81fe5c0b6b8 - Bump `ghc-prim` to version 0.4.0.0.
 - Commit 42448e3757f25735a0a5b5e2b7ee456b5e8b0039 - GHC HEAD now always looks for LLVM 3.6 specifically.

Closed tickets this past week include: #9122, #10099, #10081, #9886, #9722, #9619, #9920, #9691, #8976, #9873, #9541, #9619, #9799, #9823, #10156, #1820, #6079, #9056, #9963, #10164, #10138, #10166, #10115, #9921, #9873, #9956, #9609, #7191, #10165, #10011, #8379, #10177, #9261, #10176, #10151, #9839, #8078, #8727, #9849, #10146, #9194, #10158, #7788, #9554, #8550, #10079, #10139, #10180, #10181, #10170, #10186, #10038, #10164, and #8976.
